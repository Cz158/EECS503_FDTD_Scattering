rho=30*deltax;
phi = 0;
t = time;
E0 = 1;
A =180;
Ez_sca_m_2 = 0;
Ez_inc_m = 0;
Ez_ins = zeros(1,401);
a = rpec.*deltax;

for time = 1:1:t
    Ez_sca_m_2=0;
    for n = -A:A
    temp_t = -E0.*((1j).^-n)*(besselj(n,k.*a)).*exp(1j*n*phi)...
        .*besselh(n,2,k.*rho)./besselh(n,2,k.*a); 
    Ez_sca_m_2 = Ez_sca_m_2 + temp_t.*exp(1j*(w*deltat*time+0*pi/2));
      
    temp = E0.*(1j).^-n*besselj(n,k.*rho)*exp(1j*n*phi);
    Ez_inc_m = Ez_inc_m+temp.*exp(1j*(w*deltat*time+0*pi/2));
    end
    Ez_ins(:,time) = real(Ez_sca_m_2);
    
end


%% Frequency Domain Comparsion
%f_shadow = real(fft(Ez_shadow(500:end)));
%f_ins = real(fft(Ez_ins(500:end)));
%figure()
%plot(Ez_shadow(time/2:end))
%hold on
%plot(Ez_ins(time/2:end))

figure()
plot(f_shadow)
hold on
plot(f_ins)
title('Frequency Spectrum of Mie Series Solution and FDTD Results')
xlabel('Frequency')
ylabel('Intensity')
legend('FDTD(Shadow Region)','Mie Series(Shadow Region)')