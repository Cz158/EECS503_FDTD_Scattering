dbstop if error
%%
t = time;
E0 = 1;
Ez_sca_m_2 = 0;
Ez_inc_mie = 0;
%Ez_ins = zeros(1,401);
%E_mie = zeros(300,300);
a = rpec.*deltax;
%%
if ~exist('Ez_T')
    load('Ez_t.mat')
end
time = 1000;
Ez_avg = zeros(300,300);
%for t = floor(2*time/3):time
for t = time-100:time
    %Ez_avg = Ez_avg+(1/(0.5*time))*abs(Ez_T(:,:,t));
    Ez_avg = Ez_avg+(1/(0.5*100))*abs(Ez_T(:,:,t));
end
figure()
imagesc(Ez_avg');colormap('default');colorbar();
title('FDTD Solution of Scattering Problem')
xlabel('x(um)')
ylabel('y(um)')

%% Mie Series
A =20;
E_mie = zeros(300,300);
phi = zeros(300,300);
rho = zeros(300,300);
Ez_sca_mie = 0;
for i=1:300
    for j = 1:300
        rho(i,j) = sqrt((i-151)^2+(j-151)^2).*deltax;
        %% Phi
        if (i>151)&&(j>151)%fourth
            phi(i,j) = atan((j-151)/(i-151));
            phi(i,j) = phi(i,j)+3*pi/2;
        end
        if (i>151)&&(j<151)%third
            phi(i,j) = atan((j-151)/(i-151));
            phi(i,j) = phi(i,j)+3*pi/2;
        end
        if (i<151)&&(j>151)%first
            phi(i,j) = atan((j-151)/(i-151));
            phi(i,j) = phi(i,j)+pi/2;
        end
        if (i<151)&&(j<151)%Second
            phi(i,j) = atan((j-151)/(i-151));
            phi(i,j) = phi(i,j) + pi/2;
        end
        if (j==151)&&(i>151)
            phi(i,j) = 3*pi/2;
        end
        if (j==151)&&(i<=151)
            phi(i,j) = pi/2;
        end
        if (i==151)&&(j<151)
            phi(i,j) = pi;
        end
        if (i==151)&&(j>=151)
            phi(i,j) =0;
        end
    end
end

Ez_sca_mie = 0;
Ez_inc_mie = 0;
Ez_inc_m = 0;
Ez_t_mie = zeros(300,300,100);

A = 180;
   for n = -A:A
      temp_t = -E0.*((1j).^-n)*(besselj(n,k.*a)).*exp(1j.*n.*phi)...
              .*besselh(n,2,k.*rho)./besselh(n,2,k.*a); 
      Ez_sca_mie = Ez_sca_mie + temp_t;
      
      temp_inc = E0.*((1j).^-n)*besselj(n,k.*rho).*exp(1j.*n.*phi);
      Ez_inc_mie = Ez_inc_mie + temp_inc;

   end
Ez_sca_mie(150-15:150+15,150-15:150+15)=0;
%figure()
%imagesc((real(Ez_sca_mie)).*circ_mask,[-1,1])
%figure()
%imagesc((real(Ez_inc_mie)).*circ_mask,[-1,1])
%figure()
%imagesc(real(Ez_sca_mie+Ez_inc_mie).*circ_mask,[-1,1])

for n = 1:100
    Ez_t_mie(:,:,n)= real((Ez_sca_mie+Ez_inc_mie).*exp(1j*w*n*deltat));
end

Ez_avg_mie = zeros(300,300);
for t = 1:100
    Ez_avg_mie = Ez_avg_mie+(1/(0.5*100))*abs(Ez_t_mie(:,:,t));
end
figure()
imagesc(Ez_avg_mie.*circ_mask);colormap('default');colorbar();
title('Mie Series Solution for Scatter Problem')
xlabel('x(um)')
ylabel('y(um)')

