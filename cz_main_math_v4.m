dbstop if error

A=180;
%rho = 0:1e-6:1.5e-4-1e-6;
rho = deltax*(0:xdim/2);
phi = 0;

Ez_sca_m_1 = 0;
Ez_sca_m_2 = 0;
Ez_inc_m = 0;
E0 = 1;

isFunc = 0;
if isFunc
frequency=2*0.6e+13;%Hz
c = 3e8;
time=420;
S=1/(2^0.5);
deltax=1e-6;
deltat=S*deltax/c;
w = 2*pi*frequency;% Angular frequency
k = 2*pi*frequency/c;%wave number
a = 2.5e-05;
end

a = rpec.*deltax;

mask = zeros(1,xdim/2+1);
mask(rpec:xdim/2+1-tfsf_width)=1;

for n = -A:A
    temp_t = -E0.*((1j).^-n)*(besselj(n,k.*a)).*exp(1j*n*phi)...
        .*besselh(n,2,k.*rho)./besselh(n,2,k.*a); 
    Ez_sca_m_2 = Ez_sca_m_2 + temp_t.*exp(1j*(w*deltat*time+0*pi/2));
    
    temp_t_1 = -E0.*((1j).^-n)*(besselj(n,k.*a)).*exp(1j*n*(phi+pi))...
        .*besselh(n,2,k.*rho)./besselh(n,2,k.*a); 
    Ez_sca_m_1 = Ez_sca_m_1 + temp_t_1.*exp(1j*(w*deltat*time+0*pi/2));
    
    temp = E0.*(1j).^-n*besselj(n,k.*rho)*exp(1j*n*phi);
    Ez_inc_m = Ez_inc_m+temp.*exp(1j*(w*deltat*time+0*pi/2));
end

x= deltax*(1:xdim/2+1);
%Ez_sca_m = [0,0,0,0,fliplr(Ez_sca_m_1(155:300)),Ez_sca_m_2(151:300)];
E_sca_FDTD = (Ez-Ez_inc).*(sca_mask+1).*tfsf_mask(xdim/2,:)';
plot(x,[E_sca_FDTD(xdim/2+1:xdim,xdim/2);0],'+');
%plot(E_sca_FDTD(151,:));
hold on
%plot(real(Ez_sca_m.*mask.*tfsf_mask(150,:)))
plot(x,[real(Ez_sca_m_2)].*mask)
title('Scatter Field Ez along y=0 in TF/SF computation domain')
legend('FDTD Result','Analytical Result')
xlabel('x(um)')
ylabel('Ez(V/m)')
grid on

