%% Zhuo's FDTD program
% Boundary----PML
% Scatter Region----Cylinder or Square PEC material
% Total Field/Scatter Field Method
% Last editted on Feb.24th.2018. No bugs.
%% 
dbstop if error
clear all;
clc;
%%
isPlot = 0; %change to 1 if want to see EM field in time domain ([warning]: long running time)
isCpmpare = 0; %this part of comparsion is abandoned, don't change it to 1.
%% Grid Parameter
% Grid Dimension in x (xdim) and y (ydim) directions
xdim=300;
ydim=300;
%Total no of time steps
time=1000;
%Stability
S=1/(2^0.5);
% Permittivity, Permeability and Speed of Light
epsilon0=(1/(36*pi))*1e-9;
mu0=4*pi*1e-7;
c=3e+8;
% Spatial grid step = 1 micron meters
deltax=1*1e-6;
% Temporal grid step obtained using Courant condition
deltat=S*deltax/c;
%deltat=S*(1e-6)/c;
%% Initialization
% Initialization of field matrices
Ez=zeros(xdim,ydim);
Ezx=zeros(xdim,ydim);
Ezy=zeros(xdim,ydim);
Hy=zeros(xdim,ydim);
Hx=zeros(xdim,ydim);
% Initialization for future use.
Ez_shadow=[];%Ez at shadow region.
Ez_lit = []; %Ez at lit region.
Ez_side = [];%Ez at side region.
Ez_T = zeros(xdim,ydim,time); %The Ez field at different time
%Initialization of Permittivity and permeability martrix
epsilon=epsilon0*ones(xdim,ydim);
mu=mu0*ones(xdim,ydim);          
% Initializing electric conductivity matrices in x and y directions, part
% of them will be change to non-zero (PML) later.
sigmax=zeros(xdim,ydim);
sigmay=zeros(xdim,ydim);
%% Perfectly matched layer design
%Boundary Width
bound_width=25;
%Order of polynomial model
gradingorder=3;
R=1e-8;
%Polynomial model
sigmamax=10*(-log10(R)*(gradingorder+1)*epsilon0*c)/(2*bound_width*deltax);
%sigmamax=0;
boundfact1=((epsilon(xdim/2,bound_width)/epsilon0)*sigmamax)/((bound_width^gradingorder)*(gradingorder+1));
boundfact2=((epsilon(xdim/2,ydim-bound_width)/epsilon0)*sigmamax)/((bound_width^gradingorder)*(gradingorder+1));
boundfact3=((epsilon(bound_width,ydim/2)/epsilon0)*sigmamax)/((bound_width^gradingorder)*(gradingorder+1));
boundfact4=((epsilon(xdim-bound_width,ydim/2)/epsilon0)*sigmamax)/((bound_width^gradingorder)*(gradingorder+1));
x=0:1:bound_width;
for i=1:1:xdim
    sigmax(i,bound_width+1:-1:1)=boundfact1*((x+0.5*ones(1,bound_width+1)).^(gradingorder+1)-(x-0.5*[0 ones(1,bound_width)]).^(gradingorder+1));
    sigmax(i,ydim-bound_width:1:ydim)=boundfact2*((x+0.5*ones(1,bound_width+1)).^(gradingorder+1)-(x-0.5*[0 ones(1,bound_width)]).^(gradingorder+1));
end
for i=1:1:ydim
    sigmay(bound_width+1:-1:1,i)=boundfact3*((x+0.5*ones(1,bound_width+1)).^(gradingorder+1)-(x-0.5*[0 ones(1,bound_width)]).^(gradingorder+1))';
    sigmay(xdim-bound_width:1:xdim,i)=boundfact4*((x+0.5*ones(1,bound_width+1)).^(gradingorder+1)-(x-0.5*[0 ones(1,bound_width)]).^(gradingorder+1))';
end
% conductivity matrix
sigma_starx=(sigmax.*mu)./epsilon;
sigma_stary=(sigmay.*mu)./epsilon;
%% Source Specification 
frequency=2*0.6e+13;    %Hz
%frequency=1e+13;       %Hz
w = 2*pi*frequency;     % Angular frequency
k = 2*pi*frequency/c;   %wave number
sca = 1;                %use TF\SF method
%% Scatter Material Region
%Use PEC mask(can be modified into square)
xpec = 150;
ypec = 150;
%xpec = xdim/2;
%ypec = ydim/2;
rpec = 1 *(c/(frequency*deltax));
shape = 'c';
circ_mask = circ_mask_func(xdim,ydim,xpec,ypec,rpec,shape)';
%% TF/SF Method
%scatter field mask
tfsf_width = bound_width+5;
tfsf_mask = zeros(xdim,ydim);
tfsf_mask(tfsf_width:xdim-tfsf_width,tfsf_width:ydim-tfsf_width)=1;
% Ez_sca source mask
% For PEC, the E_total field inside PEC is always 0,so E_sca = -1*E_inc
% For dielectric ones, E_total = (1/epsilon_r)*E_inc; so E_sca = (1/epsilon_r-1)*E_inc
sca_mask = circ_mask_func(xdim,ydim,xpec,ypec,rpec,shape)';
%sca_mask(sca_mask==0)= -1;
epsilon_r = 1e20;
sca_mask(sca_mask==0)= (1/epsilon_r-1);
sca_mask(sca_mask==1)= 0;
%% Update Martrix
% H matrix update
G=((mu-0.5*deltat*sigma_starx)./(mu+0.5*deltat*sigma_starx)); 
H=(deltat/deltax)./(mu+0.5*deltat*sigma_starx);
A=((mu-0.5*deltat*sigma_stary)./(mu+0.5*deltat*sigma_stary)); 
B=(deltat/deltax)./(mu+0.5*deltat*sigma_stary);                          
%E matrix update                          
C=((epsilon-0.5*deltat*sigmax)./(epsilon+0.5*deltat*sigmax)); 
D=(deltat/deltax)./(epsilon+0.5*deltat*sigmax);   
E=((epsilon-0.5*deltat*sigmay)./(epsilon+0.5*deltat*sigmay)); 
F=(deltat/deltax)./(epsilon+0.5*deltat*sigmay);
%% Update part
for n=1:1:time
    %% Update Boundary
    n1 = 1;n2 = xdim-1 ;n11=1;n21=ydim-1;
    %% Update equation
    if sca == 0
    Ezx = Ezx.*circ_mask;
    Ezy = Ezy.*circ_mask;
    end
    %update scatter H field
    Hy(n1:n2,n11:n21)=A(n1:n2,n11:n21).*Hy(n1:n2,n11:n21)+B(n1:n2,n11:n21).*(Ezx(n1+1:n2+1,n11:n21)-Ezx(n1:n2,n11:n21)+Ezy(n1+1:n2+1,n11:n21)-Ezy(n1:n2,n11:n21));
    Hx(n1:n2,n11:n21)=G(n1:n2,n11:n21).*Hx(n1:n2,n11:n21)-H(n1:n2,n11:n21).*(Ezx(n1:n2,n11+1:n21+1)-Ezx(n1:n2,n11:n21)+Ezy(n1:n2,n11+1:n21+1)-Ezy(n1:n2,n11:n21));
    %update scatter E field
    Ezx(n1+1:n2+1,n11+1:n21+1)=C(n1+1:n2+1,n11+1:n21+1).*Ezx(n1+1:n2+1,n11+1:n21+1)+D(n1+1:n2+1,n11+1:n21+1).*(-Hx(n1+1:n2+1,n11+1:n21+1)+Hx(n1+1:n2+1,n11:n21));
    Ezy(n1+1:n2+1,n11+1:n21+1)=E(n1+1:n2+1,n11+1:n21+1).*Ezy(n1+1:n2+1,n11+1:n21+1)+F(n1+1:n2+1,n11+1:n21+1).*(Hy(n1+1:n2+1,n11+1:n21+1)-Hy(n1:n2,n11+1:n21+1));
    %% Incident Field at time n and corresponding scatter source
    Ez_inc = zeros(xdim,ydim);
    Hy_inc = zeros(xdim,ydim);
    Hx_inc = zeros(xdim,ydim);
    for i=1:xdim
            phi =  w*n*deltat-k*(i+300-xdim)*deltax;
            if phi>=0
            %if w*n*deltat-k*i*deltax>=0 %&& w*t*dt-k*j*dy<=w*150*dt
                %phi =  w*n*deltat-k*i*deltax;
                Ez_inc(i,:) = real(exp(-1i*phi));
                Hy_inc(i,:) = -(1/(c*mu0))*real(exp(-1i*phi));
            end    
    end 
    Ez_sca_sr = Ez_inc.*sca_mask;
    Hy_sca_sr = Hy_inc.*sca_mask;
    %% Source conditions
     if sca == 1 %set inside PEC to negative incident field
          Ezx(circ_mask==0) = 0*Ez_sca_sr((circ_mask==0));
          Ezy(circ_mask==0) = 1*Ez_sca_sr((circ_mask==0));
          Hy(circ_mask==0) = 1*Hy_sca_sr((circ_mask==0));
          Hx(circ_mask==0) = 0;%0.5*Ez_sca_sr((circ_mask==0));
     end
    % Total Field = Scatter Field+Incident Field
    Ez=Ezx+Ezy+Ez_inc.*tfsf_mask;
    Hy_tot = Hy+Hy_inc.*tfsf_mask;
    Hx_tot = Hx+Hx_inc.*tfsf_mask;
    Ez_shadow = [Ez_shadow,Ez(191,151)-Ez_inc(191,151)];
    Ez_lit = [Ez_lit,Ez(111,151)-Ez_inc(111,151)];
    Ez_side = [Ez_side,Ez(151,111)-Ez_inc(151,111)];
    Ez_T(:,:,n) = Ez;
    %% Visualization
    if isPlot
    %subplot(2,2,1)
    imagesc(deltax*1e+6*(1:1:xdim),(deltax*1e+6*(1:1:ydim))',Ez',[-1,1]);colormap(gray);colorbar;
    title(['Ez at time ',num2str(round(n*deltat*1e+15)),' fs']); 
    xlabel('x (in um)');
    ylabel('y (in um)');
    %subplot(2,2,2)
    %imagesc(deltax*1e+6*(1:1:xdim),(deltax*1e+6*(1:1:ydim))',Hy_tot',[-1/(mu0*c),1/(mu0*c)]);colormap(gray);colorbar;
    %title(['Hy at time ',num2str(round(n*deltat*1e+15)),' fs']); 
    %xlabel('x (in um)');
    %ylabel('y (in um)');
    %subplot(2,2,3)
    %imagesc(deltax*1e+6*(1:1:xdim),(deltax*1e+6*(1:1:ydim))',Hx_tot',[-1/(mu0*c),1/(mu0*c)]);colormap(gray);colorbar;
    %title(['Hy at time ',num2str(round(n*deltat*1e+15)),' fs']); 
    %xlabel('x (in um)');
    %ylabel('y (in um)');
    %subplot(2,2,4)
    %imagesc(deltax*1e+6*(1:1:xdim),(deltax*1e+6*(1:1:ydim))',sqrt((Hx_tot').^2+(Hy_tot').^2),[-1/(mu0*c),1/(mu0*c)]);colormap(gray);colorbar;
    %title(['|H| at time ',num2str(round(n*deltat*1e+15)),' fs']); 
    %xlabel('x (in um)');
    %ylabel('y (in um)');
    getframe;
    end
end
%% Compare with theoretical value
%cz_main_math_v4;
if isCpmpare
cz_main_math_v3

figure()
plot(Ez_inc(:,150).*tfsf_mask(150,:)');
hold on
plot(real(Ez_inc_m).*tfsf_mask(150,:))
title('Incident Field Ez along y=0 in TF/SF computation domain')
legend('FDTD Result','Analytical Result')
xlabel('x(100um)')
ylabel('Ez(V/m)')
grid on

figure()
%hold on
E_sca_FDTD = (Ez-Ez_inc).*(sca_mask+1).*tfsf_mask(150,:)';
%plot(E_sca_FDTD(:,150));
plot(E_sca_FDTD(151,:),'+');
hold on
plot(real(Ez_sca_m.*mask.*tfsf_mask(150,:)))
title('Scatter Field Ez along y=0 in TF/SF computation domain')
legend('FDTD Result','Analytical Result')
xlabel('x(100um)')
ylabel('Ez(V/m)')
grid on
end

%% Function
function mask = circ_mask_func(xdim,ydim,xpec,ypec,rpec,shape)
    %create a assistive mask to avoid computation cost.
    %input:
    if shape == 'c' %cylinder
        m=xdim; 
        n=ydim; 
        r=rpec;   
        m1=-m/2:m/2-1;  
        n1=-n/2:n/2-1;
        [x,y]=meshgrid(m1,n1);
        circle=(x-xpec+150).^2+(y-ypec+150).^2;   

        circ_mask=zeros(m,n);  
        circ_mask(find(circle>=r*r))=1;  
        circ_mask(find(circle<r*r))=0;  
        mask = circ_mask;
    end
    if shape == 'b' %box
        mask = ones(xdim,ydim);
        mask(xpec-rpec/2:xpec+rpec/2,ypec-rpec/2:ypec+rpec/2) = 0;
    end
end