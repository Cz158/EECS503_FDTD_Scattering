This is the documentation for EECS503- Zhuo's FDTD program.
Author:Zhuo Chen
Date:02/24/2018
E-mail:chzhuo@umich.edu


The program is tested with no bug on following platform:
*64-bit Win-10 Operating System,x64 based processor.
*Intel(R) Core(TM) i5-7400 CPU @3.00GHz with 8GB RAM.
*MatLab 2017b--Academic Use.

==========cz_main_v3_FDTD.M======================
*This file is the main simulation program. The comment in the file should be self-explanatory enough.
*Check the parameters before running. Run directly to get FDTD results.
*All parameters can be changed in this "main function".

*[Run cz_main_v3_FDTD.m before running other three programs.]
==================================================


==========cz_average.m============================
*calculate the Mie Series Solution and compare with FDTD results visually(Frequency Domain)
==================================================


==========cz_main_math_v4=========================
*Take a slice in FDTD Ez field and compare it with Mie Series in Time Domain.
*phi and rho is the angle and distance in cylinder coordinate.
*This program is not robust enough, which means if it receive different parameters, it's very likely to give wrong result. 
================================================== 


==========cz_NumericalDispersion==================
*Take a point in Shadow Region and record the Ez in Time domain. Compare the frequency in fft domain with Mie Series Solution.
*It's just a reference code which is not included in my report. It just helped me adjust the parameters I use.
==================================================